from encrypt.encryptable_item import EncryptableItem


class ConfigItem:
    SIZE = 2

    def __init__(self, name, key):
        self.name = name
        self.key = key

    def to_string(self) -> str:
        return self.name + " | " + self.key

    def save(self) -> str:
        return self.name + EncryptableItem.SPLIT + self.key + "\n"

    @classmethod
    def check_data(cls, data: list) -> bool:
        return len(data) == cls.SIZE

    @classmethod
    def create_item(cls, data: list):
        assert(cls.check_data(data))

        return ConfigItem(data[0], data[1])

