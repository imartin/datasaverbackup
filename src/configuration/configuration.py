import sys
from portage import os
from configuration.config_item import ConfigItem
from encrypt.encrypter import Encrypter


class Configuration:
    CONFIGURATION_FILENAME = "dsb.conf"

    def __init__(self, path: str, key: str):
        self.path = path
        self.full_path = self.path + "/" + self.CONFIGURATION_FILENAME
        self.config_items = []
        self.key = key

        if os.path.isfile(self.full_path):
            self.load_config_items()
        else:
            self.create()

    def create(self):
        try:
            if not os.path.exists(self.path):
                os.makedirs(self.path)
            file = open(self.full_path, "wb")
            file.close()

        except IOError:
            sys.stderr.write("\nImpossible to create the configuration file at " + self.path)
            raise IOError("Impossible to create the configuration file at " + self.path)

    def load_config_items(self):
        Encrypter.load_items(self.config_items, self.full_path, self.key, ConfigItem)

    def save_config_items(self):
        Encrypter.save_items(self.config_items, self.full_path, self.key)

    def item_exists(self, name: str) -> bool:
        for item in self.config_items:
            if item.name == name:
                self.config_items.remove(item)
                return True

        return False

    def add_config_item(self, name: str, key: str):
        assert(not self.item_exists(name))

        item = ConfigItem(name, key)
        self.config_items.append(item)

    def remove_config_item(self, name: str):
        for item in self.config_items:
            if item.name == name:
                self.config_items.remove(item)
                return

    def get_item_by_name(self, name: str):
        for item in self.config_items:
            if item.name == name:
                return item

        return None

    def size(self) -> int:
        return len(self.config_items)

    def to_string(self) -> str:
        result = ''
        i = 0
        for item in self.config_items:
            result = result + str(i) + " * " + item.to_string() + '\n'
            i = i + 1

        return result

    def get_item_by_pos(self, pos):
        if isinstance(pos, int) and pos < self.size():
            return self.config_items[pos]

        return None
