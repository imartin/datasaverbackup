from setuptools import setup

setup(
    name='DataSaverBackup',
    version='1.0',
    packages=['compression', 'utils', 'encrypt', 'metadata', 'configuration', 'dsb'],
    package_dir={'': 'src'},
    url='https://gitlab.com/imartin/datasaverbackup',
    license='GPL',
    author='Israel Martin Escalona',
    author_email='imartin@entel.upc.edu',
    description='Differential local backup',
    entry_points={
        'console_scripts': [
            'dsb = dsb.dsb:main',
        ],
    }
)
