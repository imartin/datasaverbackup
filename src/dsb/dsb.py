import argparse
import getpass
import os
import sys

# check arguments
from os.path import expanduser

from configuration.config_item import ConfigItem
from configuration.configuration import Configuration
from encrypt.chiper import Chiper
from metadata.metadata_container import MetadataContainer

version = "1.0.0.1"


def parameters_error(parser: argparse.ArgumentParser):
    parser.print_help()
    sys.exit("Invalid parameters")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--src", help="the source folder we want to encrypt", action="store")
    parser.add_argument("--dst", help="the destination folder where encrypted files are dropped", action="store")
    parser.add_argument("--workspace", help="the name of the configuration workspace", action="store")
    parser.add_argument("-v", "--version", help="retrieves all the contents of the workspace", action="store_true")
    parser.add_argument("-e", "--edit", help="edit the program configuration", action="store_true")
    parser.add_argument("-q", "--query", help="query some file inside the workspace, by filename", action="store")
    parser.add_argument("-rq", "--rquery", help="query some file inside the workspace, by obfuscated name",
                        action="store")
    parser.add_argument("-l", "--list", help="retrieves all the contents of the workspace", action="store_true")
    parser.add_argument("-w", "--write", help="replaces all the contents of the workspace", action="store")
    parser.add_argument("-d", "--debug", help="show debug information", action='store_true')
    parser.add_argument("-p", "--passwords", help="show passwords", action='store_true')
    parser.add_argument("-s", "--split", help="the size the volumes are going to be split on", action="store",
                        default="0")
    parser.add_argument("-cx", "--compression", help="the compression level passed to 7zip", action="store", type=int,
                        default="9")
    # parser.add_argument("-h", "--help", help="show the program usage")
    args = parser.parse_args()

    # version number
    if args.version:
        sys.exit(version)

    # verbose help
    if not args.edit:
        if args.workspace is None:
            parameters_error(parser)
        elif (args.src is not None and args.dst is None) or (args.src is None and args.dst is not None):
            parameters_error(parser)

    # configuration
    config_path = expanduser("~") + "/.config/dsb"
    config_key = getpass.getpass('Please, write the configuration password: ')
    if args.passwords:
        print(config_key)

    if not Chiper.password_strength(config_key, Chiper.MEDIUM_STRENGTH):
        sys.exit("Password too weak!")

    config = Configuration(config_path, config_key)

    # EDIT
    if args.edit:
        edit(config)

    # source folder
    src_folder = args.src
    if args.src is not None and not os.path.isdir(src_folder):
        sys.exit("Source folder does not exist")

    # destination folder
    dst_folder = args.dst
    if args.dst is not None and not os.path.isdir(dst_folder):
        sys.exit("Destination folder does not exist")

    # parse split argument
    if str(args.split) == "0":
        args.split = None

    # get container key
    config_item = config.get_item_by_name(args.workspace)
    container_path = config_path + "/" + args.workspace

    # QUERY
    if args.query is not None:
        container_password = config_item.key
        container = MetadataContainer(container_path, container_password)
        item = container.find_by_name(args.query)
        print(item.to_string())
        sys.exit(0)

    # REVERSE QUERY
    if args.rquery is not None:
        container_password = config_item.key
        container = MetadataContainer(container_path, container_password)
        item = container.find_by_hash(args.rquery)
        print(item.to_string())
        sys.exit(0)

    # LIST
    if args.list:
        container_password = config_item.key
        container = MetadataContainer(container_path, container_password)
        print(container.list())
        sys.exit(0)

    # WRITE
    if args.write is not None:
        container_password = config_item.key
        container = MetadataContainer(container_path, container_password)
        with open(args.write, "rb") as file:
            plain_text = file.read()
            container.replace_content(plain_text.decode('utf-8'))
            container.save_items()
            print("Contents of the workspace " + args.workspace + " updated")
        sys.exit(0)

    # ENCRYPTER
    # generate a new container key if the container is newly created
    if config_item is None:
        container_password = getpass.getpass('Please, write the password for the container ' + args.workspace + ': ')
        if args.passwords:
            print(container_password)

        if not Chiper.password_strength(container_password):
            sys.exit("Password too weak!")

        config.add_config_item(args.workspace, container_password)
    else:
        container_password = config_item.key

    # Workspace container
    container = MetadataContainer(container_path, container_password)
    container.process_folder(src_folder, dst_folder, args.compression, args.split, args.debug)
    config.save_config_items()


def choose_workspace(configuration: Configuration, msg="") -> ConfigItem:
    print(msg)
    print(configuration.to_string())

    workspace = configuration.size()
    while workspace >= configuration.size():
        result = input("Choose workspace: ")
        if result.isnumeric():
            workspace = int(result)

    return configuration.get_item_by_pos(workspace)


def option_list_workspaces(configuration: Configuration, msg: str):
    print(msg)
    print(configuration.to_string())
    input("Press enter to continue")


def option_workspace_rename(configuration: Configuration, msg: str):
    workspace = choose_workspace(configuration, msg)
    old_name = workspace.name
    workspace.name = input("Provide the new name: ")
    os.rename(configuration.path + "/" + old_name, configuration.path + "/" + workspace.name)
    configuration.save_config_items()
    print("Workspace " + old_name + " renamed to " + workspace.name)
    input("Press enter to continue")


def option_workspace_add(configuration: Configuration, msg: str):
    print(msg)
    container_name = input("Please, write the name of the container: ")
    container_password = input('Please, write the password for the container ' + container_name + ': ')

    if Chiper.password_strength(container_password):
        configuration.add_config_item(container_name, container_password)
        print("Container " + container_name + " created!")
        configuration.save_config_items()
    else:
        print("Password too weak! The container creation failed")

    input("Press enter to continue")


def option_workspace_delete(configuration: Configuration, msg: str):
    workspace = choose_workspace(configuration, msg)
    os.rename(configuration.path + "/" + workspace.name, configuration.path + "/" + workspace.name + ".removed!")
    configuration.remove_config_item(workspace.name)
    configuration.save_config_items()
    print("Workspace " + workspace.name + " removed")
    input("Press enter to continue")


def option_workspace_update_password(configuration: Configuration, msg: str):
    workspace = choose_workspace(configuration, msg)
    password = input("Provide the new password: ")
    password2 = input("Retype the new password: ")
    if password == password2:
        workspace.key = password
        configuration.save_config_items()
        print("Password of the workspace " + workspace.name + " successfully updated to " + password)
    else:
        print("Password mismatch!")

    input("Press enter to continue")


def option_configuration_update_password(configuration: Configuration, msg: str):
    print(msg)
    password = input("Provide the new password: ")
    password2 = input("Provide again the new password: ")
    if password == password2:
        configuration.key = password
        configuration.save_config_items()
        print("Configuration password successfully updated to " + password)
    else:
        print("Password mismatch!")

    input("Press enter to continue")


def option_exit(configuration: Configuration, msg: str):
    sys.exit('Configuration updated!')


def edit(configuration: Configuration):
    options = {0: option_list_workspaces,
               1: option_workspace_rename,
               2: option_workspace_add,
               3: option_workspace_delete,
               4: option_workspace_update_password,
               5: option_configuration_update_password,
               6: option_exit}
    option_str = ['List workspaces',
                  'Rename workspace',
                  'Add workspace',
                  'Remove workspace',
                  'Change workspace password',
                  'Change configuration password',
                  'Exit']

    option = 0
    option_end = len(options) - 1
    while option != option_end:
        os.system('clear')
        print("Edit options")
        for i in range(0, len(option_str)):
            print(str(i) + ": " + option_str[i])
        option = input("Choose an option: ")
        if option.isnumeric() and int(option) <= option_end:
            options.get(int(option))(configuration, option_str[int(option)])

        else:
            input('Wrong value provided. Press enter to continue')


if __name__ == '__main__':
    main()
