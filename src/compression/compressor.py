
class Compressor:
    def __init__(self, key: str, compression=9, split_size=None):
        self.mainArgs = [b'a', b'-r', b'-mx='+str(compression).encode('utf-8'), b'-mhe', b'-y', b'-p' + key.encode('utf-8')]
        if split_size is not None:
            self.mainArgs = self.mainArgs + [b'-v' + split_size.encode('utf-8')]

    def get_compress_arguments(self, src_path: str, filename: str, dst_path: str, encrypted_name):
        return self.mainArgs.copy() + [dst_path.encode('utf-8') + b'/' + encrypted_name.encode('utf-8') + b'.7z', src_path.encode('utf-8') + b'/' + filename.encode('utf-8')]

