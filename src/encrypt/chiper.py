import base64
import hashlib

from Crypto import Random
from Crypto.Cipher import AES



class Chiper:
    LOW_STRENGTH = 0
    MEDIUM_STRENGTH = 1
    HIGH_STRENGTH = 2
    PASSWORD_LENGTH = [8, 16, 32]
    BLOCK_SIZE = 16

    def __init__(self):
        pass

    def _trans(self, key: bytes):
        return hashlib.sha256(key).digest()

    def encrypt(self, message, passphrase: str):
        passphrase = self._trans(passphrase.encode('utf-8'))
        IV = Random.new().read(self.BLOCK_SIZE)
        aes = AES.new(passphrase, AES.MODE_CFB, IV)
        return base64.b64encode(IV + aes.encrypt(message.encode('utf-8')))

    def decrypt(self, encrypted, passphrase: str):
        passphrase = self._trans(passphrase.encode('utf-8'))
        encrypted = base64.b64decode(encrypted)
        IV = encrypted[:self.BLOCK_SIZE]
        aes = AES.new(passphrase, AES.MODE_CFB, IV)
        return aes.decrypt(encrypted[self.BLOCK_SIZE:])

    @staticmethod
    def obfuscate(tag: str) -> str:
        return hashlib.sha512(tag.encode('utf-8')).hexdigest()

    @classmethod
    def password_strength(cls, password: str, level=HIGH_STRENGTH) -> bool:
        return len(password) >= cls.PASSWORD_LENGTH[level]
