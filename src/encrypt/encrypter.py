import sys

from encrypt.chiper import Chiper
from encrypt.encryptable_item import EncryptableItem


class Encrypter:

    @classmethod
    def load_plain_items(cls, plain_text: str, container: list, encryptable_classname: EncryptableItem):
        items_text = plain_text.split("\n")

        for item in items_text:
            data = item.split(EncryptableItem.SPLIT)
            if encryptable_classname.check_data(data):
                container.append(encryptable_classname.create_item(data))

    @classmethod
    def load_items(cls, container: list, path: str, key: str, encryptable_classname: EncryptableItem):
        container.clear()
        cipher = Chiper()
        with open(path, "rb") as f:
            encrypted_text = f.read()

        if len(encrypted_text) > 0:
            try:
                plain_text = cipher.decrypt(encrypted_text, key).decode('utf-8')
            except Exception as e:
                sys.exit("Error: password mismatch!")

            cls.load_plain_items(plain_text, container, encryptable_classname)


    @classmethod
    def save_items(cls, container: list, path: str, key: str):
        plain_text = ""
        for item in container:
            plain_text = plain_text + item.save()

        cipher = Chiper()
        encrypted_text = cipher.encrypt(plain_text, key)
        with open(path, "wb") as f:
            f.write(encrypted_text)
