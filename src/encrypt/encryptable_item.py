class EncryptableItem:
    SPLIT = " | "

    def __init__(self):
        pass

    def save(self):
        raise NotImplementedError()

    def check_data(self):
        raise NotImplementedError()

    def create_new_item(self):
        raise NotImplementedError()