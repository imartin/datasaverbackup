from encrypt.encryptable_item import EncryptableItem


class MetadataItem(EncryptableItem):
    SIZE = 4

    def __init__(self, name: str, length: int, date: str, hash: str):
        self.name = name
        self.length = length
        self.date = date
        self.hash = hash

    def save(self) -> str:
        # return self.name + "\n" + str(self.length) + "\n" + self.date + "\n" + self.hash + "\n"
        return self.to_string()

    def to_string(self) -> str:
        return self.name + EncryptableItem.SPLIT + str(self.length) + EncryptableItem.SPLIT + self.date + EncryptableItem.SPLIT + self.hash + "\n"

    @classmethod
    def create_item(cls, data: list):
        assert(cls.check_data(data))

        return MetadataItem(data[0], int(data[1]), data[2], data[3])

    @classmethod
    def check_data(cls, data: list) -> bool:
        return len(data) == cls.SIZE
