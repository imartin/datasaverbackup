# -*- coding: cp1252 -*-
import glob
import sys

from os import listdir

from os.path import isdir

import time
from subprocess import Popen, PIPE
from portage import os

from compression.compressor import Compressor
from encrypt.encrypter import Encrypter
from metadata.metadata_item import MetadataItem
from encrypt.chiper import Chiper
from utils.repeater_timer import RepeaterTimer


class MetadataContainer:
    def __init__(self, path: str, key: str):
        self.path = path
        self.key = key
        self.container = []

        sys.getfilesystemencoding = lambda: 'UTF-8'

        if os.path.isfile(path):
            self.load_items()
        else:
            self.create()

    def create(self):
        try:
            folder = os.path.dirname(os.path.abspath(self.path))
            if not os.path.exists(folder):
                os.makedirs(folder)

            file = open(self.path, "wb")
            file.close()
        except IOError:
            sys.stderr.write("Impossible to create the container file at " + self.path)
            raise IOError("Impossible to create the container file at " + self.path)

    def load_items(self):
        Encrypter.load_items(self.container, self.path, self.key, MetadataItem)

    def save_items(self):
        Encrypter.save_items(self.container, self.path, self.key)

    def find_by_name(self, tag: str) -> MetadataItem:
        for item in self.container:
            if item.name == tag:
                return item
        return None

    def find_by_hash(self, tag: str) -> MetadataItem:
        for item in self.container:
            if item.hash == tag:
                return item
        return None

    def list(self) -> str:
        item_list = ""
        for item in self.container:
            item_list = item_list + item.to_string()

        return item_list

    def list_what_is_new(self, path):
        assert(isdir(path))
        out = [f.decode('utf-8') for f in listdir(path.encode('utf-8')) if self.is_new(f.decode('utf-8'))]

        return out

    def is_new(self, filename: str) -> bool:
        for item in self.container:
            if item.name == filename:
                return False
        return True

    def process_folder(self, src_folder: str, dst_folder: str, compression=9, split_size=None, debug=False):
        new_files = self.list_what_is_new(src_folder)
        compressor = Compressor(self.key, compression, split_size)
        num_files = len(new_files)

        try:
            for i in range(0, num_files):
                filename = new_files[i]
                file_path = src_folder + '/' + filename
                file_length = os.path.getsize(file_path)
                file_date = time.ctime(os.path.getmtime(file_path))
                file_hash = Chiper.obfuscate(filename)
                args = compressor.get_compress_arguments(src_folder, filename, dst_folder, file_hash)
                run_args = [b'7za'] + args
                if debug:
                    print(run_args)

                print('Processing', filename, " (", i+1, "/", num_files, ")\t", end="", flush=True)

                # Remove the file if exists
                for file in glob.glob(dst_folder + "/" + file_hash + "*"):
                    if os.path.isfile(file):
                        os.remove(file)

                timer = RepeaterTimer(5, self._print_advance)
                timer.start()
                with Popen(run_args, stdin=PIPE, stdout=PIPE, stderr=PIPE) as process:
                    while process.poll() is None:
                        process.wait()

                    timer.cancel()
                    if process.returncode == 0:
                        print("[DONE]")
                        self.container.append(MetadataItem(filename, file_length, file_date, file_hash))
                    else:
                        print("[FAIL]")

        except KeyboardInterrupt:
            print("Execution aborted. Exiting...")
        finally:
            self.save_items()

    def _print_advance(self):
        print('.', end="", flush=True)

    def replace_content(self, plain_text: str):
        self.container.clear()
        Encrypter.load_plain_items(plain_text, self.container, MetadataItem)

